# Rognage de l'image depuis le centre jusqu'à l'angle cropAngle en degrés
import sys
import os
import numpy as np
import tqdm as tq
from PIL import Image
import cv2
import argparse
from h5_info import H5Info
from h5_info.logger import Logger
from h5_info.errors import DataError
from h5_info import constants
parser = argparse.ArgumentParser()
parser.add_argument("filepath", type=str,
                    help="path to the file containing RGB Images to crop")
parser.add_argument("json_metadata", type=str,
                    help="path to csv metadata file")
parser.add_argument("output", type=str,
                    help="path to the output file")
parser.add_argument("-croph", type=int, default = 25,
                    help="Nadir crop Angle")
parser.add_argument("-cropw", type=int, default = 25,
                    help="45° crop Angle")
parser.add_argument("-ccdh", type=float, default = 0.00768,
                    help="CCD Height")
parser.add_argument("-ccdw", type=float, default = 0.01152,
                    help="CCD Width")
args = parser.parse_args()

h5_info = H5Info()

json_metadata = args.json_metadata

h5_info.load_metadata(json_metadata)

print(json_metadata)

for i in h5_info.sensors:
    if i.description == "camera_2":
       fc_length = i.focal_length
filepath = args.filepath
output = args.output
if not os.path.exists(output):
    os.makedirs(output)
filepath = filepath if filepath[-1]=="/" else filepath + "/"
output = output if output[-1]=="/" else output + "/"   
ccdh = args.ccdh
ccdw = args.ccdw
crop_angleh = args.croph
crop_anglew = args.cropw
crop_angleh  *= (np.pi / 180.0) / 2
crop_anglew  *= (np.pi / 180.0) / 2
fc_length    *= 0.001
 

for img in tq.tqdm([msk for msk in os.listdir(filepath) if msk.endswith((".jpeg", ".jpg", ".png", ".tif"))], desc="Crop RGB Images"):
    imag = cv2.imread(filepath + img)    
    (h, w)      = imag.shape[:2]
    center_x    = int(w / 2)
    center_y    = int(h / 2)
    h_cropped   = 2 * int((fc_length * np.tan(crop_angleh) * h)/ (ccdh))
    w_cropped   = 2 * int((fc_length * np.tan(crop_anglew) * w)/ (ccdw))
    cropped_img = np.copy(imag)
    cropped_img = cropped_img[center_y - int(h_cropped / 2):center_y + int(h_cropped / 2), \
                center_x - int(w_cropped / 2):center_x + int(w_cropped / 2)]
    cv2.imwrite(output + 'cropped_' + img, cropped_img)                               