def build() {

    stage('Parsing version') {
        VERSION = sh(
			script: "git describe",
			returnStdout: true
			).trim()
        echo("Version=${VERSION}")
    }

    stage('Docker build') {
        sh('echo "Docker build"')
        sh("docker build -t crop-rgb-literal:${VERSION} -f Dockerfile-crop-literal .")
    }
     stage('Docker test') {
        sh('echo "Docker test"')
        sh("docker run --rm -v ${WORKSPACE}/tests:/data -w /data crop-literal:${VERSION} rgb rgb/uplot_358_rgb_metadata.json output ")
    }
}

return this
